#!/usr/bin/env python3
# -*- coding: utf-8 -*-
"""
Created on Wed Feb 16 2022

@author: richard
"""

import sys
import discord    
from wordle_game import Game
from wordle_picture import wordle_img_path, history_img_path
from collections import defaultdict

if(len(sys.argv) > 1):
    corpus_path = sys.argv[1]
else:
    corpus_path = 'corpus'
guilds = defaultdict(lambda : Game(corpus_path))
    
if(len(sys.argv) > 2):
    font = sys.argv[2]
else:
    font = 'AbyssinicaSIL-Regular.ttf'

client = discord.Client()
    
@client.event
async def on_ready():
    print('We have logged in as {0.user}'.format(client))
    
@client.event
async def on_message(message):
    if message.author == client.user:
        return

    if message.content.startswith('!new_wordle'):
        split_message = message.content.split()
        if(len(split_message) < 2):
            word_length = 5
        else:
            word_length = int(split_message[1])
        try:
            guilds[message.guild].new_random_game(word_length)
            print(f'Started new wordle in server {message.guild}: {guilds[message.guild].word}')
            #await message.channel.send('Started new wordle!')
            game = guilds[message.guild]
            history = game.history if len(game.history) > 0 else [(' '*len(game.word), '?'*len(game.word))]
            img_path = history_img_path('/tmp', history, game.yellow_letters, game.white_letters, font)
            with open(img_path, 'rb') as f:
                picture = discord.File(f)
                await message.channel.send(file=picture)
        except Exception as e:
            print(f'Exception: {e}')
            await message.channel.send(e)

    if message.content.startswith('!new_custom_wordle'):
        split_message = message.content.split()
        if(len(split_message) < 2):
            await message.channel.send('You must give a valid word after the command')
        else:
            guilds[message.guild].new_game(split_message[1])
            print(f'Started new custom wordle in server {message.guild}: {guilds[message.guild].word}')
            #await message.channel.send('Started new custom wordle!')
            game = guilds[message.guild]
            history = game.history if len(game.history) > 0 else [(' '*len(game.word), '?'*len(game.word))]
            img_path = history_img_path('/tmp', history, game.yellow_letters, game.white_letters, font)
            with open(img_path, 'rb') as f:
                picture = discord.File(f)
                await message.channel.send(file=picture)

    if message.content.startswith('!guess'):
        guess = message.content.split()[1].upper()
        try:
            #word, color_code = guilds[message.guild].guess(guess)
            #img_path = wordle_img_path(word, color_code, guilds[message.guild].yellow_letters, guilds[message.guild].white_letters)
            
            game = guilds[message.guild]
            game.guess(guess)
            history = game.history if len(game.history) > 0 else [(' '*len(game.word), '?'*len(game.word))]
            img_path = history_img_path('/tmp', history, game.yellow_letters, game.white_letters, font)
            
            with open(img_path, 'rb') as f:
                picture = discord.File(f)
                await message.channel.send(file=picture)
        except Exception as e:
            print(f'Exception: {e}')
            await message.channel.send(e)
        except:
            print("Unknown exception?")

    if message.content.startswith('!status'):
        if(not guilds[message.guild].in_game):    
            await message.channel.send('Not currently in game')
        else:
            game = guilds[message.guild]
            history = game.history if len(game.history) > 0 else [(' '*len(game.word), '?'*len(game.word))]
            img_path = history_img_path('/tmp', history, game.yellow_letters, game.white_letters, font)
            with open(img_path, 'rb') as f:
                picture = discord.File(f)
                await message.channel.send(file=picture)
            

    if message.content.startswith('!cheat'):
        #await message.channel.send(f'Answer is: {guilds[message.guild].word}')
        answer = guilds[message.guild].word
        img_path = wordle_img_path('/tmp', answer, 'g'*len(answer), guilds[message.guild].yellow_letters, guilds[message.guild].white_letters, font)
        with open(img_path, 'rb') as f:
            picture = discord.File(f)
            print(f'{message.author} in {message.guild} is a filthy cheater!')
            await message.channel.send(file=picture)

    if(message.content.startswith('!help') or message.content.startswith('!commands')):
        await message.channel.send("""
!new_wordle <length>
    Starts a new wordle. An optional word length can be given. If absent, it defaults to 5.
!new_custom_wordle [word]
    Starts a new wordle with the given word as the answer. 
    This command can be run in a private channel of the same server to prevent the word from being shown in the chat.
!guess [guess]
    Submits a guess for the current wordle.
!status
    Lists all the previous guesses
!cheat
    A forbidden command only used by the most heinous of criminals.""")

client.run('OTQzNTE3MTE0NTY2ODM2MjU1.Yg0MyQ.8i3AHa_06a4dbovfCC7gESN50hE')