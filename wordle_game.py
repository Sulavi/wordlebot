#!/usr/bin/env python3
# -*- coding: utf-8 -*-
"""
Created on Wed Feb 16 17:59:16 2022

@author: richard
"""

import re
import random
from os.path import exists

class Game():
    def __init__(self, corpus_path = 'corpus'):
        self.in_game = False
        self.corpus_path = corpus_path
    
    def new_game(self, word):
        self.word = word.upper()
        self.white_letters = {'A', 'B', 'C', 'D', 'E', 'F', 'G', 'H', 'I', 'J', 
                             'K', 'L', 'M', 'N', 'O', 'P', 'Q', 'R', 'S', 'T', 
                             'U', 'V', 'W', 'X', 'Y', 'Z'}
        self.yellow_letters = set()
        self.history = []
        self.in_game = True
        
    def new_random_game(self, len):
        if(not exists(f'{self.corpus_path}/corpus_{str(len)}.txt')):
            raise Exception('Invalid word length. Length can be from 3 to 16')
        with open(f'{self.corpus_path}/corpus_{str(len)}.txt') as f:
            words = f.readlines();
            self.new_game(random.choice(words)[:-1])
        
    # Returns an array of (word, color_code)
    def get_history(self):
        return self.history
    
    # Returns a string with black letters
    def get_letters(self):
        return (self.yellow_letters, self.white_letters)
    
    # Return: string with color codes, byrg
    def guess(self, word):
        if(not self.in_game):
            raise Exception('Game not started')
        
        if(len(word) != len(self.word)):
            raise Exception(f'Invalid word length, guess should be {len(self.word)} letters')
        
        color_code = ''
        word = word.upper()
        for idx in range(len(word)):
            try:
                self.white_letters.remove(word[idx])
            except KeyError as e:
                pass
            if(word[idx] == self.word[idx]):
                color_code += 'g'
                self.yellow_letters.add(word[idx])
            elif(word[idx] not in self.word):
                color_code += 'b'
            else:
                # yellow letters
                self.yellow_letters.add(word[idx])
                truth_idxs = [i.start() for i in re.finditer(word[idx], self.word)]
                guess_idxs = [i.start() for i in re.finditer(word[idx], word)]
                green_idxs = list(set(truth_idxs) & set(guess_idxs))
                for green in green_idxs:
                    truth_idxs.remove(green)
                    guess_idxs.remove(green)
                
                yellow_count = min(len(truth_idxs), len(guess_idxs))
                if(guess_idxs.index(idx) >= yellow_count):
                    color_code += 'b'
                else:
                    color_code += 'y'
        self.history.append((word, color_code))
        return (word, color_code)
        