#!/usr/bin/env python3
# -*- coding: utf-8 -*-
"""
Created on Fri Feb 18 07:13:17 2022

@author: richard
"""

from PIL import Image, ImageDraw, ImageFont

def make_letter_picture(yellow_letters, white_letters, font, text_size = 16, margin = 5):
    yellow_str = ''.join(sorted(''.join(yellow_letters)))
    white_str = ' ' + ''.join(sorted(''.join(white_letters)))
    
    fnt = ImageFont.truetype(font, text_size)
    txt_width, txt_height = fnt.getsize(yellow_str + white_str)
    
    img = Image.new('RGB', (2*margin + txt_width, 2*margin + txt_height), color = 'white')
    d = ImageDraw.Draw(img)
    d.text((margin, margin), yellow_str, fill = 'green', font = fnt)
    d.text((margin + fnt.getsize(yellow_str)[0], margin), white_str, fill = 'gray', font = fnt)
    return img    

def make_square(letter, color_code, font, square_size = (40, 40), text_size = 32):
    if(color_code == 'b'):
        color = 'gray'
    elif(color_code == 'g'):
        color = 'green'
    elif(color_code == 'y'):
        color = 'gold'
    else:
        color = 'black'
    
    fnt = ImageFont.truetype(font, text_size)
    
    letter_size = fnt.getsize(letter)
    text_location = (square_size[0]/2 - letter_size[0]/2, 0)
    
    square = Image.new('RGB', square_size, color = color)
    d = ImageDraw.Draw(square)
    d.text(text_location, letter, fill = 'white', font = fnt)
    
    if(color == 'black'):
        # Add border
        border_size = 5
        d.rectangle((border_size-1, border_size-1, square_size[0]-border_size, square_size[1]-border_size), fill = 'white')
    
    return square

def make_wordle_picture(word, color_code, font, square_size = (40, 40), margin = 5):
    width = len(word) * (square_size[0] + margin) + margin
    img = Image.new('RGB', (width, square_size[1] + 2*margin), color = 'white')
    for i in range(len(word)):
        img.paste(make_square(word[i], color_code[i], font), (margin + (square_size[0] + margin)*i, margin))
    return img

def wordle_img_path(path, word, color_code, yellow_letters, white_letters, font):
    wordle_img = make_wordle_picture(word, color_code, font)
    letter_img = make_letter_picture(yellow_letters, white_letters, font)
    width = max(wordle_img.size[0], letter_img.size[0])
    height = wordle_img.size[1] + letter_img.size[1]
    img = Image.new('RGB', (width, height), color = 'white')
    img.paste(wordle_img, (0,0))
    img.paste(letter_img, (0, wordle_img.size[1]))
    img.save(f'{path}/wordle.png')
    return f'{path}/wordle.png'

def make_history_picture(history, font):
    word_size = make_wordle_picture(history[0][0], history[0][1], font).size
    history_size = (word_size[0], len(history) * word_size[1])
    history_img = Image.new('RGB', history_size)
    for i in range(len(history)):
        img = make_wordle_picture(history[i][0], history[i][1], font)
        history_img.paste(img, (0, i*word_size[1]))
    return history_img

def history_img_path(path, history, yellow_letters, white_letters, font):
    wordle_img = make_history_picture(history, font)
    letter_img = make_letter_picture(yellow_letters, white_letters, font)
    width = max(wordle_img.size[0], letter_img.size[0])
    height = wordle_img.size[1] + letter_img.size[1]
    img = Image.new('RGB', (width, height), color = 'white')
    img.paste(wordle_img, (0,0))
    img.paste(letter_img, (0, wordle_img.size[1]))
    img.save(f'{path}/wordle.png')
    return f'{path}/wordle.png'

    