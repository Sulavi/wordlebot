# README #

This bot allows you to create and play wordles of any length in a discord server

* !new_wordle <length>
	* Starts a new wordle. An optional word length can be given. If absent, it defaults to 5.
* !new_custom_wordle [word]
    * Starts a new wordle with the given word as the answer. 
    * This command can be run in a private channel of the same server to prevent the word from being shown in the chat.
* !guess [guess]
    * Submits a guess for the current wordle.
* !status
    * Lists all the previous guesses
* !cheat
    * A forbidden command only used by the most heinous of criminals.

### Usage ###

python3 wordle_boy.py <path_to_corpus_folder> <path_to_ttf_font>

* corpus folder defaults to ./corpus
* font defaults to AbyssinicaSIL-Regular.ttf

### Dependencies ###

* python3
* discord.py
* Pillow

### Known Issues ###

* When the last word in a corpus is selected, the final letter is cut off
    * This is because usually the final charater in the list is a newline and i cut it off by default
* No response when the !cheat command is used without an active game
* Direct messages to the bot share the same game
* Guesses are not checked to see if they are valid words

### Future Work ###

* Move the client secret outside of the source code
* Better reporting of the available letters, such as separating yellow and green letters and showing the letters no longer available
* Implement slash commands
* Run in a discord application
* Allow starting new wordles from direct messages in other channels
* Have games unique to a channel


